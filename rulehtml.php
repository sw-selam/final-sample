<html>
<head>
	<title>Rule</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="stylee.css">
</head>
<body>
	<div id="cc">
<p>
	<img src="image/mb.JPg" width="300" height="300" align="center">
	
	Tax rates and the standard rate cut-off point
Tax is charged as a percentage of your income. The percentage that you pay depends on the amount of your income. The first part of your income, up to a certain amount, is taxed at 20%. This is known as the standard rate of tax and the amount that it applies to is known as the standard rate tax band.

The remainder of your income is taxed at the higher rate of tax, 40% in 2017.

The amount that you can earn before you start to pay the higher rate of tax is known as your standard rate cut-off point. See case studies for an example of how to calculate income using tax rates and the standard rate cut-off point.

Standard rate cut-off points

2012, 2013 and 2014	2015, 2016 and 2017
20%	41%	20%	40%
Single person	€32,800	Balance	€33,800	Balance
Married couple/civil partners, one income	€41,800	Balance	€42,800	Balance
Married couple/civil partners, two incomes	Up to €65,600
(increase limited to the amount of

the second income - see example below)

Balance	Up to €67,600
(increase limited to the amount of the

second income - see example below)

Balance
One parent family	€36,800	Balance	€37,800	Balance
Example of standard rate cut-off point for a married couple or civil partners with two incomes
In 2017, the standard rate cut-off point for a married couple or civil partners is €42,800. If both are working, this amount is increased by the lower of the following:

€24,800 or
The amount of the income of the spouse or civil partner with the smaller income
If one person is earning €48,000 and their spouse or civil partner is earning €25,000:

The standard rate cut off point for the couple is €42,800 plus €24,800. The increase in the standard rate band is not transferable between spouses or civil partners, so the first spouse or civil partner's tax bands would be calculated as €42,800 @ 20% = €8,560 and €5,200 @ 40% = €2,080. The second spouse or civil partner's tax bands would be calculated as €24,800 @ 20% = €4,960 and €200 @ 40% = €80
</p>
</div>
</div>
 <div id="footer">
<ul>
	<i>
<li><a href="home.html">HOME/</a></li>
<li><a href="about.html">about us/</a></li>
<li><a href="customerRegester.html">customer registration/</a></li>
<li><a href="customerlogin.html"> customer login/</a></li>
<li><a href="adminlogin.html">admin login/</a></li>
<li><a href="contact.html">contact us/</a></li>
</i>
</ul>
</div>  
</body>
</html>